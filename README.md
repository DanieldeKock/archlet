
# Game of Life

> Archlet web-app coding challenge.

## The Challenge

Write a simple version of the [game of life](https://en.wikipedia.org/wiki/Conway's_Game_of_Life) with the stack ES6+TSX, React and Typescript.

This challenge should take you a couple of hours and your are free to choose which feature to implement. There is no time pressure. Just notify us by email when you are done.

Please use styled components to style your components as it is what we use in our main application.

This challenge is also for us to see how you test your code. 100% testing is what we aim for in archlet but the qualilty of the tests matters more than the quantity. The testing library is up to you and what you are comfortable with.

We are going to look at your code in terms of simplicity, structure, style and testing abilities. This includes how the commits are structured and what the commit message style looks like.

Please also write an imaginary todo list in this repository of what could be changed or added in the future.

Bonus points for fancy optimization tricks - let your mind free.
