import '@testing-library/jest-dom';
import { render, fireEvent, screen } from '@testing-library/react';
import App from './App';
import useGameOfLife, { GameResult } from './use-game-of-life';
jest.mock('./use-game-of-life');

const mockedUseGameOfLife = useGameOfLife as jest.Mock<GameResult>;
const start = jest.fn();
const reset = jest.fn();
const stop = jest.fn();
const reseed = jest.fn();

beforeEach(() => {
    mockedUseGameOfLife.mockClear();
    start.mockClear();
    reset.mockClear();
    stop.mockClear();
    reseed.mockClear();
});

const numCols = 2;
const generation = 0;

test('Render App Initial State', async () => {
    mockedUseGameOfLife.mockImplementation(() => ([
            { started: false, cells: [false,true,false,true], numCols, generation },
            { start, stop, reseed, reset }
        ]) );
    render(<App />);

    const startButton = screen.getByTestId('start-button');
    const reseedButton = screen.getByTestId('reseed-button');
    expect(startButton).toBeInTheDocument();
    expect(reseedButton).toBeInTheDocument();

    expect(screen.getByText('Generation: 0')).toBeInTheDocument();
    expect(screen.queryByTestId('stop-button')).not.toBeInTheDocument();

    expect(screen.getByTestId('grid')).toHaveStyle(`grid-template-columns: repeat(${2},20px)`);

    const cells = await screen.findAllByTestId('cell', { exact: false });
    expect(cells.length).toBe(4);
    expect(cells[0]).toHaveStyle('backgroundColor: #eee');
    expect(cells[1]).toHaveStyle('backgroundColor: #003366');
    expect(cells[2]).toHaveStyle('backgroundColor: #eee');
    expect(cells[3]).toHaveStyle('backgroundColor: #003366');

    fireEvent.click(startButton);
    expect(start).toHaveBeenCalledTimes(1);
    fireEvent.click(reseedButton);
    expect(reseed).toHaveBeenCalledTimes(1);
});

test('Render App in running state', () => {
    mockedUseGameOfLife.mockImplementation(() => ([
        { started: true, cells: [], numCols, generation },
        { start, stop, reseed, reset }
    ]) );
    render(<App />);

    const stopButton = screen.getByTestId('stop-button');
    expect(stopButton).toBeInTheDocument();
    
    expect(screen.queryByTestId('start-button')).not.toBeInTheDocument();

    fireEvent.click(stopButton);
    expect(stop).toHaveBeenCalledTimes(1);
});