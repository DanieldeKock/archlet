import { memo } from 'react';
import styled from 'styled-components';

const Cell = memo((props: {alive: boolean, className?: string}) =>
  <div
    className={props.className}
    data-testid={`cell-${props.alive ? 'live' : ''}`}
  />);

const StyledCell = styled(Cell)`
  width: 20px;
  height: 20px;
  background-color: ${props => props.alive ? '#003366' : '#eee'};
  border: 1px solid black;
`;

export default memo(StyledCell);