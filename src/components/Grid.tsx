import styled from 'styled-components';
import Cell from './Cell';

const Grid = (props: { numCols: number, cells:Array<boolean>, className?: string }) =>
  <div
    className={props.className}
    data-testid='grid'
  >
    {props.cells.map((cell, i) => <Cell key={`cell-${i}`} alive={cell}/>)}
  </div>

const StyledGrid = styled(Grid)`
    display: grid;
    grid-template-columns: repeat(${props => props.numCols}, 20px);
`;

export default StyledGrid;