import { useState, useRef } from 'react';
import GameOfLife, { IGameOfLife } from './game-of-life';

export type Options = {
    numCols?: number,
    interval?: number,
    seedProbabiliy?: number,
    game?: IGameOfLife
}

type Action = () => void

export type GameState = {
    numCols: number,
    cells: Array<boolean>, 
    generation:number,
    started:  boolean
}

export type GameActions = {
    start: Action,
    stop: Action,
    reset: Action,
    reseed: Action
};

export type GameResult = [GameState, GameActions];

const useGeneration = (game: IGameOfLife, seedProbabiliy?: number): [number, Action] => {
    const [{ generation }, setGen] = useState(() => {
        game.seed(seedProbabiliy);
        return { generation: game.generation }; // please note this has to be boxed to ensure a re-render
      });
    const setGeneration = () => setGen({ generation: game.generation });
    return [generation, setGeneration];
};

const useGame = (options?: Options) =>
    useRef(options?.game || new GameOfLife(options?.numCols || 20)).current;

const useGameOfLife = (options?: Options): GameResult => {
    const gameOfLife = useGame(options);
    const { numCols, cells } = gameOfLife;;
    const [generation, setGeneration] = useGeneration(gameOfLife, options?.seedProbabiliy);
    const [started, setStarted] = useState<NodeJS.Timeout | null>(null);
   
    const start = () => {
      if (!started) {
        const interval = setInterval(() => {
            gameOfLife.nextGeneration();
            setGeneration();
        }, options?.interval || 300);
  
        setStarted(interval);
      }
    };

    const stop = () => {
        if (started) {
            clearInterval(started);
            setStarted(null);
        }
    };

    const reset = () => {
        stop();
        gameOfLife.reset();
        setGeneration();
    };

    const reseed = () => {
        stop();
        gameOfLife.seed();
        setGeneration();
    };

    return [
        { numCols, cells, generation, started: !!started },
        { start, stop, reset, reseed }
    ];
};

export default useGameOfLife;