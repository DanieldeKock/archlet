import { renderHook, act } from '@testing-library/react-hooks';
import useGameOfLife from './use-game-of-life';

test('initial render', ()=> {
    const { result: { current: [
        { cells, generation, numCols, started },
        { start, stop, reseed }
    ] } } = renderHook(useGameOfLife);

    expect(cells.length).toBe(20*20);
    expect(generation).toBe(0);
    expect(numCols).toBe(20);
    expect(started).toBeFalsy();

    expect(typeof start).toBe('function');
    expect(typeof stop).toBe('function');
    expect(typeof reseed).toBe('function');
});

const useFastGameOfLife = () => useGameOfLife({ interval: 10, numCols: 3, seedProbabiliy: 0 })

test('start and stop', async () => {
    const { result, waitForNextUpdate } = renderHook(useFastGameOfLife);
    let previousGen = [...result.current[0].cells];

    const testCurrent = (started: boolean, generation: number) => {
        expect(result.current[0].generation).toBe(generation);
        expect(result.current[0].started).toBe(started);
    };

    const testNextGenWasCalled = () => {
        expect(result.current[0].cells).not.toEqual(previousGen);
        previousGen = [...result.current[0].cells];
    };

    testCurrent(false, 0);
    
    act(() => {
        result.current[1].start();
    });
    
    await waitForNextUpdate();
    testCurrent(true, 1);
    testNextGenWasCalled();

    await waitForNextUpdate();
    testCurrent(true, 2);
    testNextGenWasCalled();

    act(() => {
        result.current[1].stop();
    });
    
    testCurrent(false, 2);
    expect(result.current[0].cells).toEqual(previousGen);
});

test('start and reseed', async () => {
    const { result, waitForNextUpdate } = renderHook(useFastGameOfLife);
    let previousGen = [...result.current[0].cells];

    const testCurrent = (started: boolean, generation: number) => {
        expect(result.current[0].generation).toBe(generation);
        expect(result.current[0].started).toBe(started);
    };

    const testNextGenWasCalled = () => {
        expect(result.current[0].cells).not.toEqual(previousGen);
        previousGen = [...result.current[0].cells];
    };

    testCurrent(false, 0);

    act(() => {
        result.current[1].start();
    });
    
    await waitForNextUpdate();
    testCurrent(true, 1);
    testNextGenWasCalled();

    act(() => {
        result.current[1].reseed();
    });
    
    testCurrent(false, 0);
    testNextGenWasCalled();
});

test('start and reset', async () => {
    const { result, waitForNextUpdate } = renderHook(useFastGameOfLife);
    let previousGen = [...result.current[0].cells];

    const testCurrent = (started: boolean, generation: number) => {
        expect(result.current[0].generation).toBe(generation);
        expect(result.current[0].started).toBe(started);
    };

    const testNextGenWasCalled = () => {
        expect(result.current[0].cells).not.toEqual(previousGen);
        previousGen = [...result.current[0].cells];
    };

    testCurrent(false, 0);

    act(() => {
        result.current[1].start();
    });
    
    await waitForNextUpdate();
    testCurrent(true, 1);
    testNextGenWasCalled();

    act(() => {
        result.current[1].reset();
    });
    
    testCurrent(false, -1);
    testNextGenWasCalled();
});