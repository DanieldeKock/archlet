import GameOfLife from './game-of-life';

const expectedArray = (alive: boolean, cols: number = 2) => {
  const expected = [];
  for(let i = 0; i < cols*cols; i++) {
    expected.push(alive);
  }
  return expected;
};

const allDead = (cols: number = 2) => expectedArray(false);
const allAlive = (cols: number = 2) => expectedArray(true);

test('new GameOfLife initializes properties', () => {
    const game = new GameOfLife(3);
    expect(game.numCols).toBe(3);
    expect(game.numCells).toBe(3*3);
    expect(game.generation).toBe(-1);
    expect(game.cells).toEqual([]);
});

test('reset set all cells to dead', () => {
  const game = new GameOfLife(2);
  game.seed(-1);
  game.reset();
  expect(game.generation).toBe(-1);
  expect(game.cells).toEqual(allDead());
});

test('reseed sets all cells to alive', () => {
  const game = new GameOfLife(2);
  game.seed(-1);
  expect(game.generation).toBe(0);
  expect(game.cells).toEqual(allAlive());
});

test('setting of cells', () => {
  const game = new GameOfLife(3);
  game.cells = allAlive();
  expect(game.generation).toBe(0);
  expect(game.numCols).toBe(2);
  expect(game.numCells).toBe(2*2);
  expect(game.cells).toEqual(allAlive());
});

test('Any live cell with two or three live neighbours survives', () => {
  const game = new GameOfLife(2);
  game.seed(-1);
  game.nextGeneration();
  game.nextGeneration();
  expect(game.generation).toBe(2);
  expect(game.cells).toEqual(allAlive());

  game.cells = [
    false, true, false,
    true, true, true,
    false, true, false
  ];

  game.nextGeneration();

  let expected = [
    true, true, true,
    true, false, true,
    true, true, true
  ];

  expect(game.cells).toEqual(expected);
});

test('Any dead cell with three live neighbours becomes a live cell.', () => {
  const game = new GameOfLife(3);

  game.cells = [
    false, false, false,
    false, false, true,
    false, true, true
  ];
  
  game.nextGeneration();

  let expected = [
    false, false, false,
    false, true, true,
    false, true, true
  ];

  expect(game.cells).toEqual(expected);

  game.cells = [
    false, true, false,
    true, false, true,
    false, true, false
  ];

  game.nextGeneration();

  expected = [
    false, true, false,
    true, false, true,
    false, true, false
  ];

  expect(game.cells).toEqual(expected);
})