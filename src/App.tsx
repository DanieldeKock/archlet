import useGameOfLife from './use-game-of-life';
import Grid from './components/Grid';
import Row from './components/Row';

const App = () => {
  const [
    { cells, generation, numCols, started },
    { start, stop, reseed }
  ] = useGameOfLife();

  return (
    <div className="App">
      <Row>
        {!started && <button onClick={start} data-testid='start-button'>Start</button>}
        {started && <button onClick={stop} data-testid='stop-button'>Stop</button>}
        <button onClick={reseed} data-testid='reseed-button'>Reseed</button>
      </Row>
      <p>Generation: {generation}</p>
      <Grid numCols={numCols} cells={cells} />
    </div>
  );
}

export default App;
