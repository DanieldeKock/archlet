export interface IGameOfLife {
    numCols: number;
    numCells: number;
    generation: number;
    cells: Array<boolean>;
    reset(): void;
    seed(probabilty?: number): void;
    nextGeneration(): void
}

const neighbourXY = [
    [-1, -1], //south west
    [0, -1], //south
    [1, -1], //south east
    [-1, 0], //west
    [1, 0], //east
    [-1, 1], //north west
    [0, 1], //north
    [1, 1] //north east
];

export default class GameOfLife implements IGameOfLife {
    private _numCols: number;
    private _numCells: number;
    private _generation: number = -1;
    private _cells: Array<boolean> = [];
    
    constructor(numCols: number) {
        this._numCols = numCols;
        this._numCells = numCols * numCols;
    }

    get generation() {
        return this._generation;
    }

    get numCols() {
        return this._numCols;
    }

    get numCells() {
        return this._numCells;
    }

    get cells() {
        return this._cells;
    }

    set cells(c: Array<boolean>){
        this._cells = c;
        this._generation = 0;
        this._numCells = this._cells.length;
        this._numCols = Math.sqrt(this._cells.length);
    }

    reset() {
        this._cells = Array.from<boolean>({ length: this._numCells }).fill(false);
        this._generation = -1;
    }

    seed(probabilty: number = 0.5) {
        for (let i = 0; i < this._numCells; i++) {
            this._cells[i] = Math.random() > probabilty;
        }
        this._generation = 0;
    };

    private gridToIndex(x: number, y: number){
        return x + (y * this.numCols);
    }

    private isAlive(x: number, y:number) {
        if (x < 0 || x >= this._numCols || y < 0 || y >= this._numCols){
            return 0;
        }

        return this._cells[this.gridToIndex(x, y)] ? 1 : 0;
    }

    private countAliveNeighbours(x: number, y: number) {
        return neighbourXY.reduce((num, [xx, yy]) => num + this.isAlive(x+xx, y+yy), 0);
    }

    nextGeneration() {
        const changedCells: Array<number> = [];
        
        for (let x = 0; x < this.numCols; x++) {
            for (let y = 0; y < this.numCols; y++) {
                const numAlive = this.countAliveNeighbours(x, y);
                const currentCellIndex = this.gridToIndex(x, y);
                const currentCell = this.cells[currentCellIndex];

                if (currentCell && (numAlive < 2 || numAlive > 3))
                    changedCells.push(currentCellIndex);
                else if (!currentCell && numAlive === 3)
                    changedCells.push(currentCellIndex);
            }
        }

        changedCells.forEach(index => {
            this._cells[index] = !this._cells[index]
        });

        this._generation += 1;
    }
}