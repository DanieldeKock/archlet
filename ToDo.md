1. Implement the ability for Users to click and create their own seeding pattern
1.1 This will work on top of the reset functionality
2. The Usercan update Game of Life settings/config
2.1 Grid Size, this includes creating rectangles (not just squares)
2.2 Interval between updating generations
2.3 Colours of alive cells
3. Ability to tracks "age" of cells and show stats for avg age and max age
3.1 Show differnt colours as cell ages
4. Refactor GameOfLife to have more functional components
4.1 Allow undoing or back tracking of Generations